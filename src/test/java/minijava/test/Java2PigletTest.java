package minijava.test;

import minijava.jtb.MiniJavaParser;
import minijava.jtb.ParseException;
import minijava.jtb.syntaxtree.Node;
import minijava.phase.ClassBuilder;
import minijava.phase.ClassScanner;
import minijava.phase.CodeGenerator;
import minijava.state.CompilerState;
import minijava.state.SemanticErrorException;
import org.junit.Assert;
import org.junit.Test;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;

public class Java2PigletTest {

    private File pgi;
    private String code;

    public Java2PigletTest() throws IOException {
        pgi = File.createTempFile("pgi", ".jar");
        pgi.deleteOnExit();
        code = null;
        try (InputStream pgiRes = getClass().getResourceAsStream("/pgi.jar")) {
            Files.copy(pgiRes, pgi.getAbsoluteFile().toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
        System.out.printf("Piglet interpreter: %s\n", pgi.getAbsolutePath());
    }

    private boolean generatePiglet(String resName) {
        System.out.print("======= Generating code: ");
        System.out.print(resName);
        System.out.println(" =======");

        try (InputStream in = getClass().getResourceAsStream(resName)) {

            Node root = new MiniJavaParser(in).Goal();
            CompilerState compilerState = new CompilerState();

            ClassScanner classScanner = new ClassScanner();
            root.accept(classScanner, compilerState);
            compilerState.completeClassScan();

            ClassBuilder classBuilder = new ClassBuilder();
            compilerState.clearState();
            root.accept(classBuilder, compilerState);
            compilerState.completeClassBuild();

            CodeGenerator codeGenerator = new CodeGenerator();
            compilerState.clearState();
            root.accept(codeGenerator, compilerState);

            compilerState.generateClassConstructors();
            compilerState.generateRuntimeFunctions();
            code = compilerState.code.toString();
        } catch (IOException | SemanticErrorException | ParseException e) {
            e.printStackTrace();
            return false;
        }

        return true;
    }

    private boolean runAndCheck(String resName) {
        System.out.print("======= Test with answer: ");
        System.out.print(resName);
        System.out.println(" =======");
        try {
            Process process = Runtime.getRuntime().exec(new String[]{"java", "-jar", pgi.getAbsolutePath()});
            PrintWriter writer = new PrintWriter(process.getOutputStream());
            writer.print(code);
            writer.close();

            try (BufferedReader reader =
                         new BufferedReader(new InputStreamReader(process.getInputStream()));
                 BufferedReader ansIn =
                         new BufferedReader(new InputStreamReader(getClass().getResourceAsStream(resName)))) {

                process.getErrorStream().close();

                StringBuilder output = new StringBuilder();
                StringBuilder answer = new StringBuilder();
                int ch;
                while ((ch = reader.read()) != -1) {
                    output.append((char) ch);
                }

                while ((ch = ansIn.read()) != -1) {
                    answer.append((char) ch);
                }

                while (process.isAlive()) {
                    try {
                        process.waitFor();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }

                String out = output.toString().replace("\r\n", "\n");
                String ans = answer.toString().replace("\r\n", "\n");
                if (out.equals(ans)) {
                    System.out.println("Output is identical.");
                    return true;
                } else {
                    System.out.println("## OUTPUT");
                    System.out.println(out);
                    System.out.println("## EXPECTED");
                    System.out.println(ans);
                    System.out.println("########");
                    return false;
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }

    }

    private void doCase(String src, String ans) {
        Assert.assertTrue(generatePiglet(src));
        Assert.assertTrue(runAndCheck(ans));
        code = null;
    }

    @Test
    public void uclaTestCases() {
        doCase("/test_ucla/QuickSort.java", "/test_ucla/QuickSort.out");
        doCase("/test_ucla/MoreThan4.java", "/test_ucla/MoreThan4.out");
        doCase("/test_ucla/BinaryTree.java", "/test_ucla/BinaryTree.out");
        doCase("/test_ucla/BubbleSort.java", "/test_ucla/BubbleSort.out");
        doCase("/test_ucla/TreeVisitor.java", "/test_ucla/TreeVisitor.out");
        doCase("/test_ucla/LinearSearch.java", "/test_ucla/LinearSearch.out");
        doCase("/test_ucla/LinkedList.java", "/test_ucla/LinkedList.out");
        doCase("/test_ucla/Factorial.java", "/test_ucla/Factorial.out");
    }

    @Test
    public void otherUclaTestCases() {
        doCase("/test_ucla/1-PrintLiteral.java", "/test_ucla/1-PrintLiteral.out");
        doCase("/test_ucla/2-Add.java", "/test_ucla/2-Add.out");
        doCase("/test_ucla/3-Call.java", "/test_ucla/3-Call.out");
        doCase("/test_ucla/4-Vars.java", "/test_ucla/4-Vars.out");
        doCase("/test_ucla/5-OutOfBounds.java", "/test_ucla/5-OutOfBounds.out");
    }

    @Test
    public void myTestCases() {
        doCase("/test_my/array.java", "/test_my/array.out");
        doCase("/test_my/arrayOut.java", "/test_my/arrayOut.out");
        doCase("/test_my/arrayOut2.java", "/test_my/arrayOut2.out");
        doCase("/test_my/arrayRef.java", "/test_my/arrayRef.out");
        doCase("/test_my/arrayNull.java", "/test_my/arrayNull.out");
        doCase("/test_my/arrayFetchNull.java", "/test_my/arrayFetchNull.out");
        doCase("/test_my/arrayLenNull.java", "/test_my/arrayLenNull.out");
        doCase("/test_my/arrayNeg.java", "/test_my/arrayNeg.out");
        doCase("/test_my/arrayNeg2.java", "/test_my/arrayNeg2.out");
        doCase("/test_my/zeroLenArray.java", "/test_my/zeroLenArray.out");
        doCase("/test_my/badArrayAlloc.java", "/test_my/badArrayAlloc.out");
        doCase("/test_my/nestedCall.java", "/test_my/nestedCall.out");
        doCase("/test_my/emptyClass.java", "/test_my/emptyClass.out");
        doCase("/test_my/calc.java", "/test_my/calc.out");
        doCase("/test_my/override.java", "/test_my/override.out");
        doCase("/test_my/callNull.java", "/test_my/callNull.out");
        doCase("/test_my/shortcut.java", "/test_my/shortcut.out");
        doCase("/test_my/evalSeq.java", "/test_my/evalSeq.out");
        doCase("/test_my/loop.java", "/test_my/loop.out");
    }
}
