class TestLoop {
    public static void main(String[] blah) {
        int i;
        int sum;

        {
            i = 1;
            sum = 0;
        }

        while (i < 101) {
            sum = sum + i;
            i = i + 1;
        }

        System.out.println(sum);

        while (i < 2) {
            System.out.println(999);
            System.out.println(999);
            System.out.println(999);
            i = 3;
        }
    }
}

