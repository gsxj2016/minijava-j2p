class TestMethod {
    public static void main(String[] args) {
        B b;
        b = (new D()).create();
        System.out.println(666);
        System.out.println(b.setA(111));
        System.out.println(9);
        System.out.println(b.setB(222));

    }
}

class D {
    B b;
    public B create() {
        return b;
    }
}

class A {
    int n;
    public int setA(int x) {
        n = x;
        return n;
    }

    public int getA() {
        return n;
    }

    public int get() {
        return 1000 + n;
    }
}

class B extends A{
    int n;
    public int setB(int x) {
        n = x;
        return n;
    }

    public int getB() {
        return n;
    }

    public int get() {
        return 2000 + n;
    }

}
