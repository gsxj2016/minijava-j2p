class ArrS {
    public static void main(String[] args) {
        int[] a;
        int[] b;
        a = (new Gen()).get();
        b = a;
        a[2] = 10;
        b[2] = 100;
        System.out.println(a[2]);
        System.out.println(b[2]);
        b = new int[5];
        b[1] = 234;
        a[1] = 123;
        System.out.println(a[1]);
        System.out.println(b[1]);
        a[8] = 555;
        b[8] = 555;
        System.out.println(b[1]);
    }
}

class Gen {
    public int[] get() {
        return new int[10];
    }
}
