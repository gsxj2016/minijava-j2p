class TestCall {
    public static void main(String[] blah) {
        B b;
        A a;
        C c;
        b = new B();
        a = new A();
        c = new C();
        System.out.println(c.get());
        System.out.println(c.set1(123));
        System.out.println(c.get());
        System.out.println(c.set2(321));
        System.out.println(c.get());
    }
}

class A {
}

class B {
    int n;
}

class C extends B{
    public int set1(int x) {
        n = x;
        return n;
    }

    public int set2(int x) {
        int n;
        n = x;
        return n;
    }

    public int get() {
        return n;
    }

}
