class TestMethod {
    public static void main(String[] args) {
        A a;
        B b;
        b = new B();
        a = b;
        System.out.println(b.setA(111));
        System.out.println(b.setB(222));

        System.out.println(a.getA());
        System.out.println(b.getA());
        System.out.println(b.getB());

        System.out.println(a.get());
        System.out.println(b.get());

        a = new A();
        System.out.println(a.setA(333));
        System.out.println(a.get());

    }
}

class A {
    int n;
    public int setA(int x) {
        n = x;
        return n;
    }

    public int getA() {
        return n;
    }

    public int get() {
        return 1000 + n;
    }
}

class B extends A{
    int n;
    public int setB(int x) {
        n = x;
        return n;
    }

    public int getB() {
        return n;
    }

    public int get() {
        return 2000 + n;
    }

}
