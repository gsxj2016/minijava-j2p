class TestCall {
    public static void main(String[] blah) {
        A a;
        int r;
        a = new A();
        r = a.init();
        r = a.add(a.add(2, a.add(3, 4)), a.add(5, 6));
        System.out.println(r);
    }
}

class A {
    int n;

    public int init() {
        n = 1;
        return 0;
    }

    public int add(int a, int b) {
        int q;
        q = n;
        n = n + 1;
        return a + (b + q);
    }
}
