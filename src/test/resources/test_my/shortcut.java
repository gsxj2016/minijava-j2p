class TestAnd {
    public static void main(String[] blah) {
        A a;
        boolean b;
        a = new A();
        System.out.println(666);
        b = (a.genFalse(1)) && (a.genTrue(2));
        if (b) System.out.println(99999); else {}
        System.out.println(666);
        b = (a.genTrue(1)) && (a.genFalse(2));
        if (b) System.out.println(99999); else {}
    }
}

class A {
    public boolean genFalse(int id) {
        System.out.println(id);
        return false;
    }

    public boolean genTrue(int id) {
        System.out.println(id);
        return true;
    }
}

