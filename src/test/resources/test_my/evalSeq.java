class TestSeq {
    public static void main(String[] blah) {
        A a;
        boolean b;
        int r;
        int[] arr;
        a = new A();
        arr = new int[10];

        System.out.println(666);
        r = (a.genThis(1)).call(a.genZero(2),a.genZero(3),a.genZero(4),a.genZero(5));

        System.out.println(666);
        r = (a.genArray(1))[(a.genZero(2))];

        System.out.println(666);
        arr[(a.genZero(1))] = a.genZero(2);
    }
}

class A {

    public int genZero(int id){
        System.out.println(id);
        return id;
    }

    public int call(int a,int b,int c,int d){
        System.out.println(666001);
        return 0;
    }

    public A genThis(int id){
        System.out.println(id);
        return this;
    }

    public int[] genArray(int id) {
        System.out.println(id);
        return (new int[10]);
    }


}

