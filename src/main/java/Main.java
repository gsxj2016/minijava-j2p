import minijava.jtb.MiniJavaParser;
import minijava.jtb.ParseException;
import minijava.jtb.Token;
import minijava.jtb.syntaxtree.Node;
import minijava.logger.Logger;
import minijava.phase.ClassBuilder;
import minijava.phase.ClassScanner;
import minijava.phase.CodeGenerator;
import minijava.state.CompilerState;
import minijava.state.SemanticErrorException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

public class Main {
    public static void main(String[] args) {
        if (args.length < 1) {
            System.err.println("Error: No input file.");
            System.exit(2);
        }

        if ("1".equals(System.getProperty("minijava_debug_log"))) {
            Logger.logging = true;
        }

        try (InputStream in = new FileInputStream(args[0])) {
            Node root = new MiniJavaParser(in).Goal();
            CompilerState compilerState = new CompilerState();

            Logger.log("=== Phase I ===");
            ClassScanner classScanner = new ClassScanner();
            root.accept(classScanner, compilerState);
            compilerState.completeClassScan();

            Logger.log("=== Phase II ===");
            ClassBuilder classBuilder = new ClassBuilder();
            compilerState.clearState();
            root.accept(classBuilder, compilerState);
            compilerState.completeClassBuild();

            Logger.log("=== Phase III ===");
            CodeGenerator codeGenerator = new CodeGenerator();
            compilerState.clearState();
            root.accept(codeGenerator, compilerState);

            compilerState.generateClassConstructors();
            compilerState.generateRuntimeFunctions();
            System.out.print(compilerState.code.toString());
        } catch (SemanticErrorException e) {
            System.err.println(e.getMessage());
            System.exit(1);
        } catch (IOException e) {
            e.printStackTrace();
            System.exit(2);
        } catch (ParseException e) {
            Token token = e.currentToken;
            System.err.printf("Syntax error at %d:%d.\n", token.beginLine, token.beginColumn);
            System.exit(1);
        }


    }
}
