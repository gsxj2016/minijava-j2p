package minijava.state;

import minijava.symbol.Method;
import minijava.symbol.Type;

import java.util.ArrayList;
import java.util.List;

public class MethodCall {
    public Method called = null;
    public List<Type> callArgTypes = new ArrayList<>();
    public final MethodCall upper;

    public MethodCall(MethodCall upper) {
        this.upper = upper;
    }
}
