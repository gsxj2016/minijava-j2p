package minijava.state;

public class SemanticErrorException extends Exception {
    public SemanticErrorException(String cls, String message) {
        super(String.format("In class '%s':\n    %s\n", cls, message));
    }

    public SemanticErrorException(int row, int col, String message) {
        super(String.format("At %d:%d:\n    %s\n", row, col, message));
    }

    public SemanticErrorException(String cls, int row, int col, String message) {
        super(String.format("In class '%s', at %d:%d:\n    %s\n", cls, row, col, message));
    }

    public SemanticErrorException(String cls, String method, int row, int col, String message) {
        super(String.format("In class '%s', method '%s', at %d:%d:\n    %s\n", cls, method, row, col, message));
    }
}
