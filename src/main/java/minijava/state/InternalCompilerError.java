package minijava.state;

public class InternalCompilerError extends Error {
    public InternalCompilerError(String message) {
        super("[BUG] " + message);
    }
}
