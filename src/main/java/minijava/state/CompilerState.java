package minijava.state;

import minijava.logger.Logger;
import minijava.symbol.Class;
import minijava.symbol.Method;
import minijava.symbol.Type;
import minijava.symbol.Variable;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.IntStream;

public class CompilerState {
    public final Map<String, Class> classes = new HashMap<>();
    public final Map<String, Type> types = new HashMap<>();
    public Method mainFunction = null;
    public Class mainClass = null;
    private int methodUniqueId = 0;
    private int currentLabelId = 0;

    public Class currentClass = null;
    public Method currentMethod = null;
    public Type currentType = null;
    public Type lastResultType = null;
    public LocalVariables local = null;
    public MethodCall call = null;
    public ExpectedToken expected = ExpectedToken.NONE;

    public StringBuilder code;

    public int indentDepth = 0;
    private boolean hideIndent = false;

    private Variable lastVariable = null;
    private boolean lastIsField = false;
    public boolean expectedLValue = false;

    public static enum ExpectedToken {
        NONE,
        TYPE,
        VARIABLE
    }

    public int getNextLabelId() {
        return currentLabelId++;
    }

    private void generateIndent() {
        if (!hideIndent) {
            IntStream.range(0, indentDepth).forEach(x -> code.append("    "));
        }
    }

    public void generateCodeFragment(String codeText) {
        generateIndent();
        code.append(codeText);
        code.append(' ');
        hideIndent = true;
    }

    public void generateCode(String codeText) {
        generateIndent();
        code.append(codeText);
        code.append('\n');
        hideIndent = false;
    }

    public void clearState() {
        currentClass = null;
        currentMethod = null;
        currentType = null;
        lastResultType = null;
        local = null;
        call = null;
        expected = ExpectedToken.NONE;

        code = new StringBuilder();

        indentDepth = 0;
        hideIndent = false;

        lastVariable = null;
        lastIsField = false;
        expectedLValue = false;
    }

    public void processIdentifier(int row, int col, String name) throws SemanticErrorException {
        switch (expected) {
            case NONE:
                break;
            case TYPE:
                currentType = getType(row, col, name);
                break;
            case VARIABLE:
                getVariable(row, col, name);
                lastResultType = lastVariable.type;
                if (expectedLValue) {
                    generateLValue();
                } else {
                    generateRValue();
                }
                Logger.log(String.format("----ID %s", lastResultType.name));
                break;
        }
        expected = ExpectedToken.NONE;
        expectedLValue = false;
    }

    public void addClass(int row, int col, String name, String base) throws SemanticErrorException {
        if (classes.containsKey(name)) {
            throw new SemanticErrorException(row, col,
                    String.format("Redefinition of class '%s'.", name));
        }
        classes.put(name, new Class(name, base));
        Logger.log(String.format("Defined class '%s'", name));
    }

    public void selectClass(String name) {
        Logger.log(String.format("Class %s", name));
        currentClass = classes.get(name);
        currentMethod = null;
    }

    public void selectMethod(String name) {
        Logger.log(String.format("Method %s", name));
        currentMethod = currentClass.methods.get(name);
    }

    public void addField(int row, int col, String name) throws SemanticErrorException {
        Logger.log(String.format("-- field %s %s", currentType.name, name));
        checkFieldRedefinition(row, col, name);
        currentClass.fields.put(name, new Variable(name, currentType));
        currentType = null;
    }

    public void addAndSelectMethod(int row, int col, String name) throws SemanticErrorException {
        Logger.log(String.format("-- method %s %s", currentType.name, name));
        checkMethodRedefinition(row, col, name);
        currentClass.methods.put(name, currentMethod = new Method(name, currentClass, currentType, methodUniqueId++));
        currentType = null;
    }

    public void addArgument(String name) {
        Logger.log(String.format("---- [ARG] %s %s", currentType.name, name));
        currentMethod.arguments.add(new Variable(name, currentType));
        currentType = null;
    }

    private void checkType(int row, int col, Type require, Type actual) throws SemanticErrorException {
        if (!actual.isA(require)) {
            String msg = String.format("Type mismatch. expected '%s', but found '%s'.",
                    require.name, actual.name);
            failed(row, col, msg);
        }
    }

    public void checkType(int row, int col, Type require) throws SemanticErrorException {
        checkType(row, col, require, lastResultType);
    }

    public Class getClass(int row, int col, String name) throws SemanticErrorException {
        Class cls = classes.get(name);
        if (cls == null) {
            String msg = String.format("Undefined class '%s'.", name);
            failed(row, col, msg);
        }
        return cls;
    }

    public Method getInstanceMethod(int row, int col, Type cls, String m) throws SemanticErrorException {
        if (!cls.isClass()) {
            String msg = String.format("Method call needs a class instance, but got a '%s'.", cls.name);
            failed(row, col, msg);
        }
        Method method = ((Class) cls).allMethods.get(m);
        if (method == null) {
            String msg = String.format("Class '%s' has no method '%s'.", cls.name, m);
            failed(row, col, msg);
        }
        return method;
    }

    public void checkCallArguments(int row, int col) throws SemanticErrorException {
        int s1 = call.called.arguments.size();
        int s2 = call.callArgTypes.size();
        if (s1 != s2) {
            String msg = String.format("Expected %d arguments, found %d.", s1, s2);
            failed(row, col, msg);
        }
        for (int i = 0; i < s1; i++) {
            Type actual = call.callArgTypes.get(i);
            Type require = call.called.arguments.get(i).type;
            checkType(row, col, require, actual);
        }
    }

    public Variable addLocalVariable(int row, int col, String name) throws SemanticErrorException {
        if (local.v.containsKey(name)) {
            String msg = String.format("Redefinition of local variable '%s'.", name);
            failed(row, col, msg);
        }
        Variable v = new Variable(name, currentType);
        if (currentType != Type.UNUSED) {
            v.id = local.newLocal();
        }
        local.v.put(name, v);
        Logger.log(String.format("defined local var %s %s [TEMP %d]", v.type.name, v.name, v.id));
        currentType = null;
        return v;
    }

    public void completeClassScan() throws SemanticErrorException {
        types.put("boolean", Type.BOOLEAN);
        types.put("int", Type.INT);
        types.put("int[]", Type.ARRAY_OF_INTS);
        for (Class c : classes.values()) {
            scanClass(c);
            types.put(c.name, c);
        }
    }

    public void completeClassBuild() throws SemanticErrorException {
        for (Class c : classes.values())
            buildClass(c);
    }

    public void generateClassConstructors() {
        classes.values().forEach(this::generateConstructor);
    }

    public void generateRuntimeFunctions() {
        generateRTAllocateArray();
        generateRTArrayAssign();
        generateRTArrayLookup();
        generateRTArrayLength();
    }

    public void generateMethodCallPre(int instanceTemp) {
        generateCode("BEGIN");
        ++indentDepth;
        generateCodeFragment(String.format("MOVE TEMP %d", instanceTemp));
    }

    public void generateMethodCallCheck(int instanceTemp, int tableTemp, int methodTemp, int methodId) {
        int okLabel = getNextLabelId();
        generateCode("");
        generateCode(String.format("CJUMP LT TEMP %d 1 L%d", instanceTemp, okLabel));
        generateCode("ERROR");
        generateCode(String.format("L%d  HLOAD TEMP %d TEMP %d 0", okLabel, tableTemp, instanceTemp));
        generateCode(String.format("HLOAD TEMP %d TEMP %d %d", methodTemp, tableTemp, methodId));
        generateCodeFragment(String.format("RETURN CALL TEMP %d ( TEMP %d", methodTemp, instanceTemp));
    }

    public void generateMethodCallPost() {
        generateCode(")");
        --indentDepth;
        generateCodeFragment("END");
    }

    private void generateRTAllocateArray() {
        int okLabel = getNextLabelId();
        int doneLabel = getNextLabelId();
        int startLabel = getNextLabelId();
        generateCode("RT_AllocateArray [ 1 ]");
        generateCode("BEGIN");
        ++indentDepth;
        generateCode(String.format("CJUMP LT TEMP 0 0 L%d", okLabel));
        generateCode("ERROR");
        generateCode(String.format("L%d  MOVE TEMP 1 HALLOCATE PLUS 4 TIMES 4 TEMP 0", okLabel));
        generateCode("HSTORE TEMP 1 0 TEMP 0");
        // fill zero
        generateCode("MOVE TEMP 2 0");
        generateCode("MOVE TEMP 3 4");
        generateCode(String.format("L%d  CJUMP LT TEMP 2 TEMP 0 L%d", startLabel, doneLabel));
        generateCode("HSTORE PLUS TEMP 1 TEMP 3 0 0");
        generateCode("MOVE TEMP 2 PLUS TEMP 2 1");
        generateCode("MOVE TEMP 3 PLUS TEMP 3 4");
        generateCode(String.format("JUMP L%d", startLabel));
        // done
        generateCode(String.format("L%d  NOOP", doneLabel));
        generateCode("RETURN TEMP 1");
        --indentDepth;
        generateCode("END");
        generateCode("");
    }

    private void generateArrayBoundCheck() {
        // TEMP0 is array, TEMP1 is index, load array size to TEMP2
        int errorLabel = getNextLabelId();
        int secondLabel = getNextLabelId();
        int startLabel = getNextLabelId();
        generateCode(String.format("CJUMP LT TEMP 0 1 L%d", secondLabel));
        generateCode(String.format("L%d  ERROR", errorLabel));
        generateCode(String.format("L%d  CJUMP LT TEMP 1 0 L%d", secondLabel, startLabel));
        generateCode("ERROR");
        generateCode(String.format("L%d  HLOAD TEMP 2 TEMP 0 0", startLabel));
        generateCode(String.format("CJUMP LT TEMP 1 TEMP 2 L%d", errorLabel));
    }

    private void generateRTArrayLookup() {
        generateCode("RT_ArrayLookup [ 2 ]");
        generateCode("BEGIN");
        ++indentDepth;
        generateArrayBoundCheck();
        generateCode("HLOAD TEMP 3 PLUS TEMP 0 TIMES 4 TEMP 1 4");
        // done
        generateCode("RETURN TEMP 3");
        --indentDepth;
        generateCode("END");
        generateCode("");
    }

    private void generateRTArrayAssign() {
        generateCode("RT_ArrayAssign [ 2 ]");
        generateCode("BEGIN");
        ++indentDepth;
        generateArrayBoundCheck();
        // done
        generateCode("RETURN PLUS TEMP 0 TIMES 4 TEMP 1"); // offset in HLOAD should be 4
        --indentDepth;
        generateCode("END");
        generateCode("");
    }

    private void generateRTArrayLength() {
        int okLabel = getNextLabelId();
        generateCode("RT_ArrayLength [ 1 ]");
        generateCode("BEGIN");
        ++indentDepth;
        generateCode(String.format("CJUMP LT TEMP 0 1 L%d", okLabel));
        generateCode("ERROR");
        generateCode(String.format("L%d  HLOAD TEMP 1 TEMP 0 0", okLabel));
        // done
        generateCode("RETURN TEMP 1");
        --indentDepth;
        generateCode("END");
        generateCode("");
    }

    private void generateConstructor(Class c) {
        if (c == mainClass) return;
        generateCode(String.format("%s [ 0 ]", c.constructorName));
        generateCode("BEGIN");
        ++indentDepth;
        generateCode(String.format("MOVE TEMP 0 HALLOCATE %d", c.classSize));
        c.allFields
                .values()
                .forEach(x -> generateCode(String.format("HSTORE TEMP 0 %d 0", x.id)));
        if (c.methodTableSize > 0) {
            generateCode(String.format("MOVE TEMP 1 HALLOCATE %d", c.methodTableSize));
            generateCode("HSTORE TEMP 0 0 TEMP 1");
            c.allMethods
                    .values()
                    .forEach(x -> generateCode(String.format("HSTORE TEMP 1 %d %s", x.methodId, x.methodName)));
        } else {
            generateCode("HSTORE TEMP 0 0 0");
            if (!c.allMethods.isEmpty())
                throw new InternalCompilerError("bad method table size, that's strange!");
        }
        generateCode("RETURN TEMP 0");
        --indentDepth;
        generateCode("END");
        generateCode("");
    }

    private void scanClass(Class c) throws SemanticErrorException {
        if (c.scanned) return;
        if (c.scanning) {
            throw new SemanticErrorException(c.name,
                    "Circular inheritance is not allowed.");
        }
        c.scanning = true;
        if (c.baseName != null) {
            Class base = classes.get(c.baseName);
            if (base == null) {
                throw new SemanticErrorException(c.name,
                        String.format("Undefined base class '%s'.", c.baseName));
            }
            scanClass(base);
            c.base = base;
            Logger.log(String.format("Class '%s' extends class '%s'", c.name, c.base.name));
        } else {
            c.base = null;
            Logger.log(String.format("Class '%s' doesn't extend any class", c.name));
        }

        c.scanned = true;
    }

    private void buildClass(Class c) throws SemanticErrorException {
        if (c.built) return;
        if (c.base != null) {
            buildClass(c.base);
            c.base.allMethods.values().forEach((m) -> c.allMethods.put(m.name, m));
            c.base.allFields.values().forEach((v) -> c.allFields.put(v.name, v));
            c.classSize = c.base.classSize;
            c.methodTableSize = c.base.methodTableSize;
        }

        Logger.log(String.format("Build class '%s'...", c.name));
        for (Variable v : c.fields.values()) {
            c.allFields.put(v.name, v);
            v.id = c.classSize;
            c.classSize += 4;
            Logger.log(String.format("-- new field %s %s [%d]", v.type.name, v.name, v.id));
        }

        for (Method m : c.methods.values()) {
            Method o = c.allMethods.get(m.name);
            if (o != null) {
                if (!m.canOverride(o)) {
                    throw new SemanticErrorException(c.name,
                            String.format("Invalid override of method '%s'," +
                                    " overridden methods should have" +
                                    " same arguments and return type.", m.name));
                }
                m.over = o;
                m.methodId = o.methodId;
                Logger.log(String.format("-- method '%s' overrides '%s::%s' [%d]",
                        m.name, m.over.parent.name, m.over.name, m.methodId));
            } else {
                m.methodId = c.methodTableSize;
                c.methodTableSize += 4;
                Logger.log(String.format("-- new method '%s' [%d]", m.name, m.methodId));
            }
            c.allMethods.put(m.name, m);
        }

        c.built = true;
    }

    private void checkFieldRedefinition(int row, int col, String name) throws SemanticErrorException {
        if (currentClass.fields.containsKey(name)) {
            throw new SemanticErrorException(currentClass.name, row, col,
                    String.format("Redefinition of field '%s'.", name));
        }
    }

    private void checkMethodRedefinition(int row, int col, String name) throws SemanticErrorException {
        if (currentClass.methods.containsKey(name)) {
            throw new SemanticErrorException(currentClass.name, row, col,
                    String.format("Redefinition of method '%s'.", name));
        }
    }

    private Type getType(int row, int col, String name) throws SemanticErrorException {
        Type type = types.get(name);
        if (type == null) {
            String msg = String.format("Undefined type '%s'.", name);
            failed(row, col, msg);
        }
        return type;
    }

    private void getVariable(int row, int col, String name) throws SemanticErrorException {
        Variable v = local.v.get(name);
        lastIsField = false;
        if (v == null && currentMethod != mainFunction) {
            v = currentClass.allFields.get(name);
            lastIsField = true;
        }
        if (v == null) {
            String msg = String.format("Undefined variable '%s'.", name);
            failed(row, col, msg);
        }
        lastVariable = v;
    }

    public void failed(int row, int col, String msg) throws SemanticErrorException {
        if (currentClass != null && currentMethod != null) {
            throw new SemanticErrorException(currentClass.name, currentMethod.name, row, col, msg);
        } else if (currentClass != null) {
            throw new SemanticErrorException(currentClass.name, row, col, msg);
        } else {
            throw new SemanticErrorException(row, col, msg);
        }
    }

    private void generateLValue() {
        if (lastIsField) {
            generateCodeFragment(String.format("HSTORE TEMP 0 %d", lastVariable.id));
        } else {
            generateCodeFragment(String.format("MOVE TEMP %d", lastVariable.id));
        }
    }

    private void generateRValue() {
        if (lastIsField) {
            int fieldTemp = local.newLocal();
            generateCodeFragment(String.format("BEGIN HLOAD TEMP %d TEMP 0 %d RETURN TEMP %d END",
                    fieldTemp,
                    lastVariable.id,
                    fieldTemp));
        } else {
            generateCodeFragment(String.format("TEMP %d", lastVariable.id));
        }
    }

}
