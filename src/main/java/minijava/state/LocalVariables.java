package minijava.state;

import minijava.symbol.Variable;

import java.util.HashMap;
import java.util.Map;

public class LocalVariables {
    public Map<String, Variable> v = new HashMap<>();
    private int count = 0;

    public int newLocal() {
        return count++;
    }
}
