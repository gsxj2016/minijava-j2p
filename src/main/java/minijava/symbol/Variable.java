package minijava.symbol;

import minijava.state.InternalCompilerError;

public class Variable {
    public final String name;
    public final Type type;
    public int id;

    public Variable(String name, Type type) {
        this.name = name;
        this.type = type;
        if (type == null) {
            throw new InternalCompilerError("variable type should not be null.");
        }
        this.id = -1;
    }
}
