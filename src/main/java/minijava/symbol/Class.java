package minijava.symbol;

import java.util.HashMap;
import java.util.Map;

public class Class extends Type {
    public final String baseName;
    public Class base = null;
    public final Map<String, Variable> fields = new HashMap<>();
    public final Map<String, Variable> allFields = new HashMap<>();
    public final Map<String, Method> methods = new HashMap<>();
    public final Map<String, Method> allMethods = new HashMap<>();
    public int classSize;
    public int methodTableSize;
    public String constructorName;

    public boolean scanning = false;
    public boolean scanned = false;
    public boolean built = false;

    public Class(String name, String baseName) {
        super(name);
        this.baseName = baseName;
        this.classSize = 4;
        this.methodTableSize = 0;
        this.constructorName = String.format("Ctor_%s", name);
    }

    public boolean isBaseOf(Class d) {
        while (d != null) {
            if (d == this) return true;
            d = d.base;
        }
        return false;
    }

    @Override
    public boolean isClass() {
        return true;
    }
}
