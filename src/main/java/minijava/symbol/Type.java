package minijava.symbol;

/**
 *
 */
public abstract class Type {
    public static final Type BOOLEAN = new Type("boolean") {
    };

    public static final Type INT = new Type("int") {
    };

    public static final Type ARRAY_OF_INTS = new Type("int[]") {
    };

    public static final Type UNUSED = new Type("<UNSUPPORTED>") {
        // placeholder
    };


    public final String name;

    public Type(String name) {
        this.name = name;
    }

    public boolean isClass() {
        return false;
    }

    public boolean isA(Type b) {
        if (this.isClass() && b.isClass())
            return ((Class) b).isBaseOf((Class) this);
        return this == b;
    }
}
