package minijava.symbol;

import minijava.state.InternalCompilerError;

import java.util.ArrayList;
import java.util.List;

public class Method {
    public final String name;
    public final Class parent;
    public final Type returnType;
    public final List<Variable> arguments = new ArrayList<>();
    public Method over = null;
    public int methodId;
    public String methodName;

    public Method(String name, Class parent, Type returnType, int uniqueId) {
        this.name = name;
        this.parent = parent;
        this.returnType = returnType;
        if (parent == null || returnType == null) {
            throw new InternalCompilerError("method return type or parent class should not be null.");
        }
        this.methodId = -1;
        this.methodName = String.format("Method_%d_%s_%s", uniqueId, parent.name, name);
    }

    public boolean canOverride(Method o) {
        if (returnType != o.returnType)
            return false;

        int num = arguments.size();
        if (num != o.arguments.size())
            return false;

        for (int i = 0; i < num; ++i) {
            if (arguments.get(i).type != o.arguments.get(i).type)
                return false;
        }

        return true;
    }
}
