package minijava.phase;

import minijava.jtb.syntaxtree.*;
import minijava.jtb.visitor.GJVoidDepthFirst;
import minijava.state.CompilerState;
import minijava.state.SemanticErrorException;
import minijava.symbol.Method;

public class ClassBuilder extends GJVoidDepthFirst<CompilerState> {
    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "{"
     * f3 -> "public"
     * f4 -> "static"
     * f5 -> "void"
     * f6 -> "main"
     * f7 -> "("
     * f8 -> "String"
     * f9 -> "["
     * f10 -> "]"
     * f11 -> Identifier()
     * f12 -> ")"
     * f13 -> "{"
     * f14 -> ( VarDeclaration() )*
     * f15 -> ( Statement() )*
     * f16 -> "}"
     * f17 -> "}"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(MainClass n, CompilerState argu) throws SemanticErrorException {
        argu.selectClass(n.f1.f0.tokenImage);
        argu.mainFunction = new Method("main", argu.currentClass, minijava.symbol.Type.UNUSED, 0);
        argu.mainClass = argu.currentClass;
        argu.currentClass = null;
    }

    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "{"
     * f3 -> ( VarDeclaration() )*
     * f4 -> ( MethodDeclaration() )*
     * f5 -> "}"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ClassDeclaration n, CompilerState argu) throws SemanticErrorException {
        argu.selectClass(n.f1.f0.tokenImage);
        n.f3.accept(this, argu);
        n.f4.accept(this, argu);
        argu.currentClass = null;
    }

    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "extends"
     * f3 -> Identifier()
     * f4 -> "{"
     * f5 -> ( VarDeclaration() )*
     * f6 -> ( MethodDeclaration() )*
     * f7 -> "}"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ClassExtendsDeclaration n, CompilerState argu) throws SemanticErrorException {
        argu.selectClass(n.f1.f0.tokenImage);
        n.f5.accept(this, argu);
        n.f6.accept(this, argu);
        argu.currentClass = null;
    }

    /**
     * f0 -> Type()
     * f1 -> Identifier()
     * f2 -> ";"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(VarDeclaration n, CompilerState argu) throws SemanticErrorException {
        n.f0.accept(this, argu);
        if (argu.currentMethod == null)
            argu.addField(n.f1.f0.beginLine, n.f1.f0.beginColumn, n.f1.f0.tokenImage);
    }

    /**
     * f0 -> "public"
     * f1 -> Type()
     * f2 -> Identifier()
     * f3 -> "("
     * f4 -> ( FormalParameterList() )?
     * f5 -> ")"
     * f6 -> "{"
     * f7 -> ( VarDeclaration() )*
     * f8 -> ( Statement() )*
     * f9 -> "return"
     * f10 -> Expression()
     * f11 -> ";"
     * f12 -> "}"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(MethodDeclaration n, CompilerState argu) throws SemanticErrorException {
        n.f1.accept(this, argu);
        argu.addAndSelectMethod(n.f0.beginLine, n.f0.beginColumn, n.f2.f0.tokenImage);
        n.f4.accept(this, argu);
        argu.currentMethod = null;
    }

    /**
     * f0 -> Type()
     * f1 -> Identifier()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(FormalParameter n, CompilerState argu) throws SemanticErrorException {
        n.f0.accept(this, argu);
        argu.addArgument(n.f1.f0.tokenImage);
    }

    /**
     * f0 -> ArrayType()
     * | BooleanType()
     * | IntegerType()
     * | Identifier()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(Type n, CompilerState argu) throws SemanticErrorException {
        argu.expected = CompilerState.ExpectedToken.TYPE;
        n.f0.accept(this, argu);
        argu.expected = CompilerState.ExpectedToken.NONE;
    }

    /**
     * f0 -> "int"
     * f1 -> "["
     * f2 -> "]"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ArrayType n, CompilerState argu) throws SemanticErrorException {
        argu.currentType = minijava.symbol.Type.ARRAY_OF_INTS;
    }

    /**
     * f0 -> "boolean"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(BooleanType n, CompilerState argu) throws SemanticErrorException {
        argu.currentType = minijava.symbol.Type.BOOLEAN;
    }

    /**
     * f0 -> "int"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(IntegerType n, CompilerState argu) throws SemanticErrorException {
        argu.currentType = minijava.symbol.Type.INT;
    }

    /**
     * f0 -> <IDENTIFIER>
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(Identifier n, CompilerState argu) throws SemanticErrorException {
        argu.processIdentifier(n.f0.beginLine, n.f0.beginColumn, n.f0.tokenImage);
    }
}
