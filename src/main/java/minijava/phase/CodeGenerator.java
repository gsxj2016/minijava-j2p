package minijava.phase;

import minijava.jtb.syntaxtree.*;
import minijava.jtb.visitor.GJVoidDepthFirst;
import minijava.logger.Logger;
import minijava.state.CompilerState;
import minijava.state.LocalVariables;
import minijava.state.MethodCall;
import minijava.state.SemanticErrorException;
import minijava.symbol.Class;
import minijava.symbol.Method;
import minijava.symbol.Variable;

public class CodeGenerator extends GJVoidDepthFirst<CompilerState> {
    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "{"
     * f3 -> "public"
     * f4 -> "static"
     * f5 -> "void"
     * f6 -> "main"
     * f7 -> "("
     * f8 -> "String"
     * f9 -> "["
     * f10 -> "]"
     * f11 -> Identifier()
     * f12 -> ")"
     * f13 -> "{"
     * f14 -> ( VarDeclaration() )*
     * f15 -> ( Statement() )*
     * f16 -> "}"
     * f17 -> "}"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(MainClass n, CompilerState argu) throws SemanticErrorException {
        argu.selectClass(n.f1.f0.tokenImage);
        argu.currentMethod = argu.mainFunction;
        argu.local = new LocalVariables();

        // add placeholder for 'String[] args'
        argu.currentType = minijava.symbol.Type.UNUSED;
        argu.addLocalVariable(n.f11.f0.beginLine, n.f11.f0.beginColumn, n.f11.f0.tokenImage);

        // enter scope
        argu.generateCode("MAIN");
        ++argu.indentDepth;

        // process local variables
        n.f14.accept(this, argu);

        // process statements
        n.f15.accept(this, argu);

        // exit scope
        --argu.indentDepth;
        argu.generateCode("END");
        argu.generateCode("");

        argu.local = null;
        argu.currentMethod = null;
        argu.currentClass = null;
    }

    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "{"
     * f3 -> ( VarDeclaration() )*
     * f4 -> ( MethodDeclaration() )*
     * f5 -> "}"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ClassDeclaration n, CompilerState argu) throws SemanticErrorException {
        argu.selectClass(n.f1.f0.tokenImage);
        n.f4.accept(this, argu);
        argu.currentClass = null;
    }

    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "extends"
     * f3 -> Identifier()
     * f4 -> "{"
     * f5 -> ( VarDeclaration() )*
     * f6 -> ( MethodDeclaration() )*
     * f7 -> "}"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ClassExtendsDeclaration n, CompilerState argu) throws SemanticErrorException {
        argu.selectClass(n.f1.f0.tokenImage);
        n.f6.accept(this, argu);
        argu.currentClass = null;
    }

    /**
     * f0 -> Type()
     * f1 -> Identifier()
     * f2 -> ";"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(VarDeclaration n, CompilerState argu) throws SemanticErrorException {
        if (argu.currentMethod != null) {
            n.f0.accept(this, argu);
            Variable v = argu.addLocalVariable(n.f1.f0.beginLine, n.f1.f0.beginColumn, n.f1.f0.tokenImage);
            argu.generateCode(String.format("MOVE TEMP %d 0", v.id));
        }
    }

    /**
     * f0 -> "public"
     * f1 -> Type()
     * f2 -> Identifier()
     * f3 -> "("
     * f4 -> ( FormalParameterList() )?
     * f5 -> ")"
     * f6 -> "{"
     * f7 -> ( VarDeclaration() )*
     * f8 -> ( Statement() )*
     * f9 -> "return"
     * f10 -> Expression()
     * f11 -> ";"
     * f12 -> "}"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(MethodDeclaration n, CompilerState argu) throws SemanticErrorException {
        argu.selectMethod(n.f2.f0.tokenImage);
        argu.local = new LocalVariables();
        argu.local.newLocal(); //Temp 0 is this

        // process arguments
        n.f4.accept(this, argu);

        // enter scope
        argu.generateCode(String.format("%s [ %d ]",
                argu.currentMethod.methodName,
                argu.currentMethod.arguments.size() + 1));
        argu.generateCode("BEGIN");
        ++argu.indentDepth;

        // process local variables
        n.f7.accept(this, argu);

        // process statements
        n.f8.accept(this, argu);

        // return value
        argu.generateCodeFragment("RETURN");
        minijava.symbol.Type returnType = argu.currentMethod.returnType;
        n.f10.accept(this, argu);
        argu.checkType(n.f9.beginLine, n.f9.beginColumn, returnType);
        argu.generateCode("");

        // exit scope
        --argu.indentDepth;
        argu.generateCode("END");
        argu.generateCode("");

        argu.currentMethod = null;
        argu.local = null;
    }

    /**
     * f0 -> Type()
     * f1 -> Identifier()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(FormalParameter n, CompilerState argu) throws SemanticErrorException {
        n.f0.accept(this, argu);
        argu.addLocalVariable(n.f1.f0.beginLine, n.f1.f0.beginColumn, n.f1.f0.tokenImage);
    }

    /**
     * f0 -> ArrayType()
     * | BooleanType()
     * | IntegerType()
     * | Identifier()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(Type n, CompilerState argu) throws SemanticErrorException {
        argu.expected = CompilerState.ExpectedToken.TYPE;
        n.f0.accept(this, argu);
        argu.expected = CompilerState.ExpectedToken.NONE;
    }

    /**
     * f0 -> "int"
     * f1 -> "["
     * f2 -> "]"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ArrayType n, CompilerState argu) throws SemanticErrorException {
        argu.currentType = minijava.symbol.Type.ARRAY_OF_INTS;
    }

    /**
     * f0 -> "boolean"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(BooleanType n, CompilerState argu) throws SemanticErrorException {
        argu.currentType = minijava.symbol.Type.BOOLEAN;
    }

    /**
     * f0 -> "int"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(IntegerType n, CompilerState argu) throws SemanticErrorException {
        argu.currentType = minijava.symbol.Type.INT;
    }

    /**
     * f0 -> "{"
     * f1 -> ( Statement() )*
     * f2 -> "}"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(Block n, CompilerState argu) throws SemanticErrorException {
        Logger.log("--stat BLK");
        n.f1.accept(this, argu);
        Logger.log("--stat BLK END");
    }

    /**
     * f0 -> Identifier()
     * f1 -> "="
     * f2 -> Expression()
     * f3 -> ";"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(AssignmentStatement n, CompilerState argu) throws SemanticErrorException {
        Logger.log("--stat ASSIGN");
        // left
        argu.expected = CompilerState.ExpectedToken.VARIABLE;
        argu.expectedLValue = true;
        n.f0.accept(this, argu);
        argu.expectedLValue = false;
        argu.expected = CompilerState.ExpectedToken.NONE;
        minijava.symbol.Type instanceType = argu.lastResultType;
        // right
        n.f2.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, instanceType);
        // done
        argu.generateCode("");
    }

    /**
     * f0 -> Identifier()
     * f1 -> "["
     * f2 -> Expression()
     * f3 -> "]"
     * f4 -> "="
     * f5 -> Expression()
     * f6 -> ";"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ArrayAssignmentStatement n, CompilerState argu) throws SemanticErrorException {
        Logger.log("--stat ARR_ASSIGN");
        argu.generateCodeFragment("HSTORE CALL RT_ArrayAssign (");
        // left
        argu.expected = CompilerState.ExpectedToken.VARIABLE;
        n.f0.accept(this, argu);
        argu.expected = CompilerState.ExpectedToken.NONE;
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.ARRAY_OF_INTS);
        // index
        n.f2.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.INT);
        argu.generateCodeFragment(") 4");
        // right
        n.f5.accept(this, argu);
        argu.checkType(n.f4.beginLine, n.f4.beginColumn, minijava.symbol.Type.INT);
        // done
        argu.generateCode("");
    }

    /**
     * f0 -> "if"
     * f1 -> "("
     * f2 -> Expression()
     * f3 -> ")"
     * f4 -> Statement()
     * f5 -> "else"
     * f6 -> Statement()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(IfStatement n, CompilerState argu) throws SemanticErrorException {
        Logger.log("--stat IF");
        int falseLabel = argu.getNextLabelId();
        int doneLabel = argu.getNextLabelId();
        // condition
        argu.generateCodeFragment("CJUMP");
        n.f2.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.BOOLEAN);
        argu.generateCode(String.format("L%d", falseLabel));
        // true statements
        n.f4.accept(this, argu);
        argu.generateCode(String.format("JUMP L%d", doneLabel));
        // false statements
        argu.generateCode(String.format("L%d  NOOP", falseLabel));
        n.f6.accept(this, argu);
        // done
        argu.generateCode(String.format("L%d  NOOP", doneLabel));
    }

    /**
     * f0 -> "while"
     * f1 -> "("
     * f2 -> Expression()
     * f3 -> ")"
     * f4 -> Statement()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(WhileStatement n, CompilerState argu) throws SemanticErrorException {
        Logger.log("--stat WHILE");
        int startLabel = argu.getNextLabelId();
        int doneLabel = argu.getNextLabelId();
        // condition
        argu.generateCodeFragment(String.format("L%d  CJUMP", startLabel));
        n.f2.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.BOOLEAN);
        argu.generateCode(String.format("L%d", doneLabel));
        // statements
        n.f4.accept(this, argu);
        // done
        argu.generateCode(String.format("JUMP L%d", startLabel));
        argu.generateCode(String.format("L%d  NOOP", doneLabel));
    }

    /**
     * f0 -> "System.out.println"
     * f1 -> "("
     * f2 -> Expression()
     * f3 -> ")"
     * f4 -> ";"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(PrintStatement n, CompilerState argu) throws SemanticErrorException {
        Logger.log("--stat PRINT");
        argu.generateCodeFragment("PRINT");
        // expression
        n.f2.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.INT);
        // done
        argu.generateCode("");
    }

    /**
     * f0 -> AndExpression()
     * | CompareExpression()
     * | PlusExpression()
     * | MinusExpression()
     * | TimesExpression()
     * | ArrayLookup()
     * | ArrayLength()
     * | MessageSend()
     * | PrimaryExpression()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(Expression n, CompilerState argu) throws SemanticErrorException {
        n.f0.accept(this, argu);
        //argu.lastResultType = argu.lastResultType;
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "&&"
     * f2 -> PrimaryExpression()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(AndExpression n, CompilerState argu) throws SemanticErrorException {
        int falseLabel = argu.getNextLabelId();
        int doneLabel = argu.getNextLabelId();
        int valueTemp = argu.local.newLocal();
        argu.generateCode("BEGIN");
        ++argu.indentDepth;
        // left
        argu.generateCodeFragment("CJUMP ");
        n.f0.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.BOOLEAN);
        argu.generateCode(String.format("L%d", falseLabel));
        // right
        argu.generateCodeFragment(String.format("MOVE TEMP %d", valueTemp));
        n.f2.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.BOOLEAN);
        argu.generateCode("");
        // done
        argu.generateCode(String.format("JUMP L%d", doneLabel));
        argu.generateCode(String.format("L%d MOVE TEMP %d 0", falseLabel, valueTemp));
        argu.generateCode(String.format("L%d NOOP", doneLabel));
        argu.generateCode(String.format("RETURN TEMP %d", valueTemp));
        --argu.indentDepth;
        argu.generateCodeFragment("END");
        argu.lastResultType = minijava.symbol.Type.BOOLEAN;
        Logger.log(String.format("----AND %s", argu.lastResultType.name));
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "<"
     * f2 -> PrimaryExpression()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(CompareExpression n, CompilerState argu) throws SemanticErrorException {
        argu.generateCodeFragment("LT");
        n.f0.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.INT);
        n.f2.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.INT);
        argu.lastResultType = minijava.symbol.Type.BOOLEAN;
        Logger.log(String.format("----CMP %s", argu.lastResultType.name));
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "+"
     * f2 -> PrimaryExpression()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(PlusExpression n, CompilerState argu) throws SemanticErrorException {
        argu.generateCodeFragment("PLUS");
        n.f0.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.INT);
        n.f2.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.INT);
        argu.lastResultType = minijava.symbol.Type.INT;
        Logger.log(String.format("----PLUS %s", argu.lastResultType.name));
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "-"
     * f2 -> PrimaryExpression()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(MinusExpression n, CompilerState argu) throws SemanticErrorException {
        argu.generateCodeFragment("MINUS");
        n.f0.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.INT);
        n.f2.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.INT);
        argu.lastResultType = minijava.symbol.Type.INT;
        Logger.log(String.format("----MINUS %s", argu.lastResultType.name));
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "*"
     * f2 -> PrimaryExpression()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(TimesExpression n, CompilerState argu) throws SemanticErrorException {
        argu.generateCodeFragment("TIMES");
        n.f0.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.INT);
        n.f2.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.INT);
        argu.lastResultType = minijava.symbol.Type.INT;
        Logger.log(String.format("----MUL %s", argu.lastResultType.name));
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "["
     * f2 -> PrimaryExpression()
     * f3 -> "]"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ArrayLookup n, CompilerState argu) throws SemanticErrorException {
        argu.generateCodeFragment("CALL RT_ArrayLookup (");
        // array
        n.f0.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.ARRAY_OF_INTS);
        // index
        n.f2.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.INT);
        // done
        argu.lastResultType = minijava.symbol.Type.INT;
        argu.generateCodeFragment(")");
        Logger.log(String.format("----ARR_REF %s", argu.lastResultType.name));
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "."
     * f2 -> "length"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ArrayLength n, CompilerState argu) throws SemanticErrorException {
        argu.generateCodeFragment("CALL RT_ArrayLength (");
        n.f0.accept(this, argu);
        argu.checkType(n.f1.beginLine, n.f1.beginColumn, minijava.symbol.Type.ARRAY_OF_INTS);
        argu.lastResultType = minijava.symbol.Type.INT;
        argu.generateCodeFragment(")");
        Logger.log(String.format("----LEN %s", argu.lastResultType.name));
    }

    /**
     * f0 -> PrimaryExpression()
     * f1 -> "."
     * f2 -> Identifier()
     * f3 -> "("
     * f4 -> ( ExpressionList() )?
     * f5 -> ")"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(MessageSend n, CompilerState argu) throws SemanticErrorException {
        int instanceTemp = argu.local.newLocal();
        int tableTemp = argu.local.newLocal();
        int methodTemp = argu.local.newLocal();

        // instance
        argu.generateMethodCallPre(instanceTemp);
        n.f0.accept(this, argu);
        minijava.symbol.Type instanceType = argu.lastResultType;

        // method
        Method method = argu.getInstanceMethod(n.f1.beginLine, n.f1.beginColumn, instanceType, n.f2.f0.tokenImage);
        argu.generateMethodCallCheck(instanceTemp, tableTemp, methodTemp, method.methodId);

        // arguments
        argu.call = new MethodCall(argu.call);
        argu.call.called = method;
        n.f4.accept(this, argu);
        argu.checkCallArguments(n.f1.beginLine, n.f1.beginColumn);
        argu.call = argu.call.upper;

        // ret
        argu.generateMethodCallPost();
        argu.lastResultType = method.returnType;
        Logger.log(String.format("----CALLING %s", argu.lastResultType.name));
    }

    /**
     * f0 -> Expression()
     * f1 -> ( ExpressionRest() )*
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ExpressionList n, CompilerState argu) throws SemanticErrorException {
        n.f0.accept(this, argu);
        argu.call.callArgTypes.add(argu.lastResultType);
        n.f1.accept(this, argu);
    }

    /**
     * f0 -> ","
     * f1 -> Expression()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ExpressionRest n, CompilerState argu) throws SemanticErrorException {
        n.f1.accept(this, argu);
        argu.call.callArgTypes.add(argu.lastResultType);
    }

    /**
     * f0 -> IntegerLiteral()
     * | TrueLiteral()
     * | FalseLiteral()
     * | Identifier()
     * | ThisExpression()
     * | ArrayAllocationExpression()
     * | AllocationExpression()
     * | NotExpression()
     * | BracketExpression()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(PrimaryExpression n, CompilerState argu) throws SemanticErrorException {
        argu.expected = CompilerState.ExpectedToken.VARIABLE;
        n.f0.accept(this, argu);
        argu.expected = CompilerState.ExpectedToken.NONE;
        //argu.lastResultType = argu.lastResultType;
    }

    /**
     * f0 -> <INTEGER_LITERAL>
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(IntegerLiteral n, CompilerState argu) throws SemanticErrorException {
        argu.generateCodeFragment(n.f0.tokenImage);
        argu.lastResultType = minijava.symbol.Type.INT;
        Logger.log(String.format("----NUM %s", argu.lastResultType.name));
    }

    /**
     * f0 -> "true"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(TrueLiteral n, CompilerState argu) throws SemanticErrorException {
        argu.generateCodeFragment("1");
        argu.lastResultType = minijava.symbol.Type.BOOLEAN;
        Logger.log(String.format("----TRUE %s", argu.lastResultType.name));
    }

    /**
     * f0 -> "false"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(FalseLiteral n, CompilerState argu) throws SemanticErrorException {
        argu.generateCodeFragment("0");
        argu.lastResultType = minijava.symbol.Type.BOOLEAN;
        Logger.log(String.format("----FALSE %s", argu.lastResultType.name));
    }

    /**
     * f0 -> <IDENTIFIER>
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(Identifier n, CompilerState argu) throws SemanticErrorException {
        argu.processIdentifier(n.f0.beginLine, n.f0.beginColumn, n.f0.tokenImage);
    }

    /**
     * f0 -> "this"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ThisExpression n, CompilerState argu) throws SemanticErrorException {
        if (argu.currentMethod == argu.mainFunction) {
            argu.failed(n.f0.beginLine, n.f0.beginColumn, "Cannot use 'this' in static methods.");
        }
        argu.generateCodeFragment("TEMP 0");
        argu.lastResultType = argu.currentClass;
        Logger.log(String.format("----THIS %s", argu.lastResultType.name));
    }

    /**
     * f0 -> "new"
     * f1 -> "int"
     * f2 -> "["
     * f3 -> Expression()
     * f4 -> "]"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ArrayAllocationExpression n, CompilerState argu) throws SemanticErrorException {
        argu.generateCodeFragment("CALL RT_AllocateArray (");
        n.f3.accept(this, argu);
        argu.checkType(n.f0.beginLine, n.f0.beginColumn, minijava.symbol.Type.INT);
        argu.lastResultType = minijava.symbol.Type.ARRAY_OF_INTS;
        argu.generateCodeFragment(")");
        Logger.log(String.format("----ARR_ALLOC %s", argu.lastResultType.name));
    }

    /**
     * f0 -> "new"
     * f1 -> Identifier()
     * f2 -> "("
     * f3 -> ")"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(AllocationExpression n, CompilerState argu) throws SemanticErrorException {
        Class c = argu.getClass(n.f0.beginLine, n.f0.beginColumn, n.f1.f0.tokenImage);
        argu.generateCodeFragment(String.format("CALL %s ( )", c.constructorName));
        argu.lastResultType = c;
        Logger.log(String.format("----ALLOC %s", argu.lastResultType.name));
    }

    /**
     * f0 -> "!"
     * f1 -> Expression()
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(NotExpression n, CompilerState argu) throws SemanticErrorException {
        argu.generateCodeFragment("MINUS 1");
        n.f1.accept(this, argu);
        argu.checkType(n.f0.beginLine, n.f0.beginColumn, minijava.symbol.Type.BOOLEAN);
        //argu.lastResultType = argu.lastResultType;
        Logger.log(String.format("----NOT %s", argu.lastResultType.name));
    }

    /**
     * f0 -> "("
     * f1 -> Expression()
     * f2 -> ")"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(BracketExpression n, CompilerState argu) throws SemanticErrorException {
        n.f1.accept(this, argu);
        //argu.lastResultType = argu.lastResultType;
        Logger.log(String.format("----BRK %s", argu.lastResultType.name));
    }
}
