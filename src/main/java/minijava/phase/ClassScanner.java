package minijava.phase;

import minijava.jtb.syntaxtree.ClassDeclaration;
import minijava.jtb.syntaxtree.ClassExtendsDeclaration;
import minijava.jtb.syntaxtree.MainClass;
import minijava.jtb.visitor.GJVoidDepthFirst;
import minijava.state.CompilerState;
import minijava.state.SemanticErrorException;

public class ClassScanner extends GJVoidDepthFirst<CompilerState> {

    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "{"
     * f3 -> "public"
     * f4 -> "static"
     * f5 -> "void"
     * f6 -> "main"
     * f7 -> "("
     * f8 -> "String"
     * f9 -> "["
     * f10 -> "]"
     * f11 -> Identifier()
     * f12 -> ")"
     * f13 -> "{"
     * f14 -> ( VarDeclaration() )*
     * f15 -> ( Statement() )*
     * f16 -> "}"
     * f17 -> "}"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(MainClass n, CompilerState argu) throws SemanticErrorException {
        argu.addClass(n.f0.beginLine, n.f0.beginColumn, n.f1.f0.tokenImage, null);
    }

    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "{"
     * f3 -> ( VarDeclaration() )*
     * f4 -> ( MethodDeclaration() )*
     * f5 -> "}"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ClassDeclaration n, CompilerState argu) throws SemanticErrorException {
        argu.addClass(n.f0.beginLine, n.f0.beginColumn, n.f1.f0.tokenImage, null);
    }

    /**
     * f0 -> "class"
     * f1 -> Identifier()
     * f2 -> "extends"
     * f3 -> Identifier()
     * f4 -> "{"
     * f5 -> ( VarDeclaration() )*
     * f6 -> ( MethodDeclaration() )*
     * f7 -> "}"
     *
     * @param n    Node
     * @param argu Environment
     */
    @Override
    public void visit(ClassExtendsDeclaration n, CompilerState argu) throws SemanticErrorException {
        argu.addClass(n.f0.beginLine, n.f0.beginColumn, n.f1.f0.tokenImage, n.f3.f0.tokenImage);
    }

}
