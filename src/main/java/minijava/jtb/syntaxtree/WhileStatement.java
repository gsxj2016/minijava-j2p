//
// Generated by JTB 1.3.2
//

package minijava.jtb.syntaxtree;

import minijava.jtb.visitor.GJVoidVisitor;
import minijava.state.SemanticErrorException;

/**
 * Grammar production:
 * f0 -> "while"
 * f1 -> "("
 * f2 -> Expression()
 * f3 -> ")"
 * f4 -> Statement()
 */
public class WhileStatement implements Node {
   public NodeToken f0;
   public NodeToken f1;
   public Expression f2;
   public NodeToken f3;
   public Statement f4;

   public WhileStatement(NodeToken n0, NodeToken n1, Expression n2, NodeToken n3, Statement n4) {
      f0 = n0;
      f1 = n1;
      f2 = n2;
      f3 = n3;
      f4 = n4;
   }

   public WhileStatement(Expression n0, Statement n1) {
      f0 = new NodeToken("while");
      f1 = new NodeToken("(");
      f2 = n0;
      f3 = new NodeToken(")");
      f4 = n1;
   }

   public <A> void accept(GJVoidVisitor<A> v, A argu) throws SemanticErrorException {
      v.visit(this,argu);
   }
}

